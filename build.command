cd "$(dirname "$0")"

# clang -framework Foundation main.m -o lockscreen
osacompile -o 'Lockie.app' 'Lockie.applescript'
cp Lockie.icns Lockie.app/Contents/Resources/applet.icns 
touch Lockie.app/Contents/Resources/applet.icns
cp lockscreen Lockie.app/Contents/Resources/Scripts/lockscreen
