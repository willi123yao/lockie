# Lockie - lock your mac
[![Build Status](https://camo.githubusercontent.com/773616b557979897f7c0df90693c23c1796d6235/68747470733a2f2f696d672e736869656c64732e696f2f7472617669732f63756265726974652f63756265726974652f6d61737465722e7376673f7374796c653d666c6174)](https://gitlab.com/willi123yao/lockie/raw/master/bin/Lockie.zip)

Quickly lock of your mac with this nifty app! 
Locking your mac is never so easy before!

## Introduction

You can double-click Lockie to lock or type "Lockie" in Spotlight to also give the same results.

## Download
To download this app, go to the bin folder and choose [Lockie.zip](https://gitlab.com/willi123yao/lockie/raw/master/bin/Lockie.zip)

## Installation

> To use of any of this code, download as zip/ pull this repository and run the `build`
> command line script. Make sure you have the necessary required libraries and compilers
> to do so.

This app is at its most basic stage and requires more features. If you have any requests or issues
with the app, proceed to the '*issues*' page and post it there.


Since not everyone has '*clang*', I have kindly provided the precompiled version of '*main.m*'
so to facilitate compilation. If you want to generate your own '*main.m*', just remove the '*#*'
from the '*build.command*' file and compile it.

References
---------------

[gaomd/lock-screen-app](https://github.com/gaomd/lock-screen-app)
